#include <gconf/gconf-client.h>
#include <gconf/gconf.h>
#include <dconf.h>

G_DEFINE_TYPE (GConfClient, gconf_client, G_TYPE_OBJECT)

static void
gconf_client_class_init (GConfClientClass *class)
{
}

static void
gconf_client_init (GConfClient *client)
{
}

GConfClient *
gconf_client_get_default (void)
{
  return g_object_new (GCONF_TYPE_CLIENT, NULL);
}

gchar *
gconf_client_get_string (GConfClient  *client,
                         const gchar  *key,
                         GError      **error)
{
  gchar *result = NULL;
  GVariant *value;
 
  {
    gchar *path = g_strjoin ("", "/user", key, NULL);
    value = dconf_get (path);
    g_free (path);
  }

  if (value != NULL) {
    result = g_variant_dup_string (value, NULL);
    g_variant_unref (value);
  }

  return result;
}

void
gconf_client_add_dir (GConfClient             *client,
                      const gchar             *dir,
                      GConfClientPreloadType   preload,
                      GError                 **error)
{
}

void
gconf_client_remove_dir (GConfClient  *client,
                         const gchar  *dir,
                         GError      **error)
{
}

gboolean
gconf_client_get_bool (GConfClient  *client,
                       const gchar  *key,
                       GError      **error)
{
  gboolean result = FALSE;
  GVariant *value;
 
  {
    gchar *path = g_strjoin ("", "/user", key, NULL);
    value = dconf_get (path);
    g_free (path);
  }

  if (value != NULL) {
    result = g_variant_get_boolean (value);
    g_variant_unref (value);
  }

  return result;
}

gboolean
gconf_client_get_int (GConfClient  *client,
                      const gchar  *key,
                      GError      **error)
{
  gint result = FALSE;
  GVariant *value;
 
  {
    gchar *path = g_strjoin ("", "/user", key, NULL);
    value = dconf_get (path);
    g_free (path);
  }

  if (value != NULL) {
    result = g_variant_get_int32 (value);
    g_variant_unref (value);
  }

  return result;
}

gboolean
gconf_client_set_string (GConfClient  *client,
                         const gchar  *key,
                         const gchar  *value,
                         GError      **error)
{
  gchar *path = g_strjoin ("", "/user", key, NULL);
  dconf_set (path, g_variant_new_string (value), NULL, error);
  g_free (path);
}

int
gconf_client_set_int (GConfClient  *client,
                      const gchar  *key,
                      gint          value,
                      GError      **error)
{
  gchar *path = g_strjoin ("", "/user", key, NULL);
  dconf_set (path, g_variant_new_int32 (value), NULL, error);
  g_free (path);
}

gboolean
gconf_client_set_bool (GConfClient  *client,
                       const gchar  *key,
                       gboolean      value,
                       GError      **error)
{
  gchar *path = g_strjoin ("", "/user", key, NULL);
  dconf_set (path, g_variant_new_boolean (value), NULL, error);
  g_free (path);
}
